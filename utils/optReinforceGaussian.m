function [PlotData, optThresh, GaussParam, ReinParam] = optReinforceGaussian(costRatios, ini_sprtBounds, Hyp, ReinParam, GaussParam)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

% Initialise output data structure
[PlotData.thresh,...
    PlotData.mean,...
    PlotData.sd]        = deal(zeros(length(ini_sprtBounds), Hyp.nTrials));
[PlotData.reward,...
    PlotData.error,...
    PlotData.decTime]   = deal(zeros(1, Hyp.nTrials));

%% Iterative learning of optimal decision
isEqualThresholds = length(ini_sprtBounds) == 1;
if isEqualThresholds
    sprtBounds = [-ini_sprtBounds ini_sprtBounds]; % Force equal magnitude thresholds
else
    sprtBounds = ini_sprtBounds;
end
reward = 0;
% Calculate baseline using exponential average as suggested in Williams
% (1992). Comment this line to remove baseline.
ReinParam.baseline = ReinParam.gamma*reward + (1 - ReinParam.gamma)*...
    ReinParam.baseline;

for trialIndex = 1:Hyp.nTrials
    % Apply SPRT decision rule to evidence indexed by trialIndex.
    [selHyp, decTime, errorType] = makeSprtDecision(Hyp, sprtBounds,...
        'decisions', 'one',...
        'trialNumber', trialIndex);
    
    % Calculate reward.
    reward = - dot(costRatios, errorType) - decTime; % Setting c=1
    %reward = - errorType - costRatios.*decTime;
    
    % REINFORCE -- Update Gaussian unit parameters.
    GaussParam = reinforceGaussian(reward, ReinParam, GaussParam);
    
    % Sample new threshold values.
    tmp = normrnd(GaussParam.mean, GaussParam.sd);
    % Constrained by equal cost ratios
    if isEqualThresholds
        sprtBounds = [-tmp tmp];
    else
        sprtBounds = tmp;
    end
    
    ReinParam.thresh = tmp;
    
    % Calculate baseline using exponential average as suggested in Williams
    % (1992). Comment this line to remove baseline.
    ReinParam.baseline = ReinParam.gamma*reward + (1 - ReinParam.gamma)*...
        ReinParam.baseline;
    
    % Capture plot data
    isError = ~isempty(find(errorType, 1));
    PlotData.thresh(:, trialIndex) = ReinParam.thresh;
    
    PlotData.decTime(trialIndex) = decTime;
    PlotData.reward(trialIndex) = reward;
    PlotData.error(trialIndex) = isError;
    PlotData.mean(:, trialIndex) = GaussParam.mean';
    PlotData.sd(:,trialIndex) = GaussParam.sd';
    
    %disp(trialIndex)
    
end

% Output mean theshold over last 100 learning trials
optThresh = mean(PlotData.thresh(:, end-100:end), 2);