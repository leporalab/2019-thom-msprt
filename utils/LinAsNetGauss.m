classdef LinAsNetGauss
    %LinAsNetGauss Linear associative network for Gaussian output units
    %   Detailed explanation goes here
    
    properties
        numBounds
        thetaMean
        thetaSd
        baseline
    end
    
    methods
        function obj = LinAsNetGauss(numBounds, sizeInputLayer)
            %LinAsNet Construct an instance of this class
            %   Detailed explanation goes here
            if nargin > 0
                obj.numBounds = numBounds;
                obj.thetaMean = rand(numBounds, sizeInputLayer)/sizeInputLayer;
                obj.thetaSd = ones(numBounds, sizeInputLayer)/sizeInputLayer;      
            end
        end
        
        function [mu, sigma] = calcMuSig(obj, contextInput)
            %METHOD1 Summary
            %   Detailed explanation here
            
            % Initialise mu and sigma vectors.
            [mu, sigma] = deal(zeros(1, obj.numBounds));
            
            % Calculate mean and sd using context and network parameters.
            for i = 1:obj.numBounds
                mu(i) = dot(obj.thetaMean(i,:), contextInput);
                sigma(i) = dot(obj.thetaSd(i,:), contextInput);
            end
        end
        
        function bounds = forward(obj, contextInput)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            
            % Calculate Gaussian paramters from context
            [mu, sigma] = calcMuSig(obj, contextInput);
            
            % Sample decision boundaries
            bounds = normrnd(mu, sigma);
        end
        
        function obj = reinforce(obj, ReinParam, reward, contextInput)
            %METHOD2 Summary
            %   Detailed explanation here
            
            % Calculate Gaussian paramters from context
            [mu, sigma] = calcMuSig(obj, contextInput, lastBounds);
            
            % Set individual mean and sd learning rates as suggested in Williams (1992).
            learningRateMean = ReinParam.learningRate*sigma.^2;
            learningRateSd = ReinParam.learningRate*sigma.^2;
            
            % Set all baselines to be equal.
            baselineMean = ReinParam.baseline;
            baselineSd = ReinParam.baseline;
            
            % Calculate eligibility vectors.
            [eVecMu, eVecSig] = deal(zeros(obj.numBounds));
            for i = 1:obj.numBounds
                eVecMu(i,:) = (1/sigma(i)^2).*(lastBounds(i) - mu(i))*contextInput;
                eVecSig(i,:) = (((lastBounds(i) - mu(i))^2) / (sigma(i)^2) - 1)*contextInput;
            end
            
            % Calculate deltas
            deltaThetaMean = learningRateMean.*(reward - baselineMean).*eVecMu;
            deltaThetaSd = learningRateSd.*(reward - baselineSd).*eVecSig;
            
            % Update thetaMean and thetaSd
            obj.thetaMean = obj.thetaMean + deltaThetaMean;
            obj.thetaSd = obj.thetaSd + deltaThetaSd;
        end
    end
end

