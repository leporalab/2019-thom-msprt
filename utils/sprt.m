function [ sel, Y, decTime, errorType ] = sprt( Hypotheses, hTrue, maxT, thresholds )
%SPRT Run SPRT method on hypotheses
%   Detailed explanation goes here

Y = zeros(maxT, 1);
sTrueArray = zeros(maxT, 1);
likelihoodAltArray = zeros(maxT, 1);
likelihoodNullArray = zeros(maxT, 1);

Y(1) = 0;
for t = 2:maxT
    sTrueArray(t) = icdf(Hypotheses(hTrue).name, rand(1), Hypotheses(hTrue).mu, Hypotheses(hTrue).sd);
    likelihoodAltArray(t) = pdf(Hypotheses(2).name, sTrueArray(t), Hypotheses(2).mu, Hypotheses(2).sd);
    likelihoodNullArray(t) = pdf(Hypotheses(1).name, sTrueArray(t), Hypotheses(1).mu, Hypotheses(1).sd);
    Y(t) = Y(t-1) + log(likelihoodAltArray(t) / likelihoodNullArray(t));
    if Y(t) > thresholds(2)
        hSelected = 2;
        break
    elseif Y(t) < thresholds(1)
        hSelected = 1;
        break
    end
end

if t == maxT
    error('Decision threshold not reached in allotted time');
end

sel = hSelected;
if sel == hTrue
    errorType = [0, 0];
elseif sel == 1 && hTrue == 2
    errorType = [0, 1];
elseif sel == 2 && hTrue == 1
    errorType = [1, 0];
end

decTime = t - 1;

end

