function [ OptThresh, varargout] = exhaustive_opt( Hypotheses, hTrue, maxT, threshRange, nTrials, costRatio )
%EXHAUSTIVE_OPT Finds optimal threshold (for equal cost ratios) for sprt model
%using exhaustive search of threshold values and gp regression
%   Detailed explanation goes here

% Duplicate costRatio for calculating reward
costRatios = [costRatio, costRatio];

% Initialise storage arrays
threshold = zeros(nTrials, 1);
reward = zeros(nTrials, 1);

disp('Generating SPRT data...')
%reverseStr = '';
% Sample nTrials thresholds and calculate rewards
t1 = tic;
r1 = threshRange(1); r2 = threshRange(2);
parfor i = 1:nTrials
    % Uniformly sample threshold from range
    threshold(i) = r1 + rand(1) * (r2 - r1);
    thresholds = [-threshold(i), threshold(i)];
    
    % Run one SPRT trial
    [ ~, ~, decisionTime, errorType ] = sprt( Hypotheses, hTrue, maxT, thresholds );
    
    % Calculate reward (inner prodect of errorType and error costs plus
    % total sampling cost)
    reward(i) = - dot( costRatios, errorType ) - decisionTime; 
   
%    percentDone = 100 * i / nTrials;
%    msg = sprintf('Percent done: %3.1f', percentDone); %Don't forget this semicolon
%    fprintf([reverseStr, msg]);
%    reverseStr = repmat(sprintf('\b'), 1, length(msg));
end
fprintf('\n')
toc(t1)

% Fit Gaussian process model to data
dataTable = table(threshold, reward);

% Find optimal threshold using GP model
t2 = tic;
disp('Fitting Gaussian Process regression model...')
gprMdl = fitrgp(dataTable, 'reward');
rewardPred = resubPredict(gprMdl);
toc(t2)

f1 = figure;
set(f1, 'color','white')
plot( threshold, reward, 'k.' )
xlabel('threshold'); ylabel('reward')
hold on
[sortedThreshold, indices] = sort(threshold);
sortedRewardPred = rewardPred(indices);
plot( sortedThreshold, sortedRewardPred, 'r', 'LineWidth', 1.5);

% Output results including plotting data if required
if nargout == 2
    varargout{1}.threshold = threshold;
    varargout{1}.reward = reward;
end

OptThresh = 1;

end

