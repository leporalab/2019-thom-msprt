function GaussParam = reinforceGaussianContext(reward, ReinParam, GaussParam, contextInput)
%REINFORCE Implements REINFORCE learning rule for Gaussian parameters
%   Detailed explanation goes here

% Set individual mean and sd learning rates as suggested in Williams (1992).
learningRateMean = ReinParam.learningRate*GaussParam.sd.^2;
learningRateSd = ReinParam.learningRate*GaussParam.sd.^2;
% learningRateMean = ReinParam.learningRate;
% learningRateSd = ReinParam.learningRate;

% Set all baselines to be equal.
baselineMean = ReinParam.baseline;
baselineSd = ReinParam.baseline;

% 

% Calculate deltas for Gaussian unit parameter values (Section 6 Williams
% (1992).
deltaMean = learningRateMean.*(reward - baselineMean).*...
    ((ReinParam.thresh - GaussParam.mean)./GaussParam.sd.^2).*GaussParam.input;
deltaSd = learningRateSd.*(reward - baselineSd).*...
    (((ReinParam.thresh - GaussParam.mean).^2./GaussParam.sd.^2) - 1).*GaussParam.input;

% Update Gaussian unit parameter values.
GaussParam.thetaMean = GaussParam.thetaMean + deltaMean;
GaussParam.thetaSd = GaussParam.thetaSd + deltaSd;

% Calculate parameterized Gaussian mean and sd.
GaussParam.mean = GaussParam.thetaMean;
GaussParam.sd = exp(GaussParam.thetaSd);

end

