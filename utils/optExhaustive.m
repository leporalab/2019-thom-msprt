function Out = optExhaustive(costRatios, threshRange, Hyp)
%EXHAUSTIVE_OPT Finds optimal threshold (for equal cost ratios) for sprt model
%using exhaustive search of threshold values and gp regression
%   Detailed explanation goes here

% Uniformly sample threshold from range.
r1 = threshRange(1); r2 = threshRange(2);
threshold = r1 + rand(Hyp.nTrials, 1)*(r2 - r1);
sprtBounds = [-threshold, threshold];

% Apply SPRT decision rule to evidence.
[~, decTime, errorType] = makeSprtDecision(Hyp, sprtBounds,...
    'decisions', 'all');

% Calculate reward (inner product of errorType and error costs plus
% total sampling cost)
reward = - errorType*costRatios' - decTime;

% Fit Gaussian process model to data
dataTable = table(threshold, reward);

% Find optimal threshold using GP model
disp('Fitting Gaussian Process regression model...')
Out.gprMdl = fitrgp(dataTable, 'reward');
Out.rewardPred = resubPredict(Out.gprMdl);
[Out.optReward, optThreshIdx] = max(Out.rewardPred);
Out.optThresh = threshold(optThreshIdx);

Out.threshold = threshold;
Out.reward = reward;
Out.decTime = decTime;
isError = ~isempty(find(errorType, 1));
Out.error = isError;

end

