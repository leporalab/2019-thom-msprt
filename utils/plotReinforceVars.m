function [f1, f2] = plotReinforceVars(PlotData, numTrialsSmoothing, plotType)
%PLOTREINFORCEVARS
%   Detailed explanation goes here

% Smooth outputs.
%numTrialsSmoothing = 50;
for i = 1:size(PlotData.thresh, 1)
    thresholdSmoothed(i,:) = smooth(PlotData.thresh(i,:), numTrialsSmoothing);
end
decisionTimeSmoothed = smooth(PlotData.decTime, numTrialsSmoothing);
rewardSmoothed = smooth(PlotData.reward, numTrialsSmoothing);
errorSmoothed = smooth(PlotData.error, numTrialsSmoothing);
trials = 1:PlotData.nTrials;

% Plot figures.
f1 = figure('Color', 'white');
numPlots = 4;
subplot(numPlots,1,1)
hold on
plot(trials, errorSmoothed )
title('Decision error'); ylabel('Decision error, e');
subplot(numPlots,1,2)
hold on
plot(trials, decisionTimeSmoothed )
title('Decision time'); ylabel('Decision time, T');
subplot(numPlots,1,3)
hold on
plot(trials, rewardSmoothed )
title('Reward'); ylabel('Reward, r');
subplot(numPlots,1,4)
hold on
plot(trials, thresholdSmoothed);
title('Decision threshold'); xlabel('trials, N'); ylabel('Decision thresholds, \theta');
%hline(ini_sprtBounds(1),'b:');

if strcmp(plotType, 'sprt')
    legend('Lower bound', 'Upper bound')
    optThresh = mean(PlotData.thresh(:, end-100:end), 2);
    t1 = annotation('textbox');
    t1.String = sprintf('REINFORCE lower bound = %.2f \nREINFORCE upper bound = %.2f',...
        [optThresh(1), optThresh(2)]);
elseif strcmp(plotType, 'msprt')
    optThresh = mean(PlotData.thresh(:, end-100:end), 2);
    t1 = annotation('textbox');
    t1.String = sprintf('REINFORCE MSPRT bound = %.2f', optThresh);
end

f2 = figure;
subplot(2,1,1)
hold on
plot(trials, PlotData.mean);
title('Gaussian \mu'); ylabel('\mu');
legend('\mu_0', '\mu_1')
subplot(2,1,2)
hold on
plot(trials, PlotData.sd);
title('Gaussian \sigma'); ylabel('\sigma');
legend('\sigma_0', '\sigma_1')

%% Plot options
f1.Color = 'white';
f1.Units               = 'centimeters';
f1.Position(1) = 2;
f1.Position(2) = 2;
%f1.Position(3)         = 18.5; % two columns
f1.Position(3)         = 9; % one column
f1.Position(4)         = 18;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);
set(t1, 'FontName', 'Arial', 'FontSize', 8,...
    'Position', [0.2365    0.0102    0.5374    0.0523]);
set(gcf, 'Renderer', 'painters')


end

