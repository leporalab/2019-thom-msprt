function [xopt, yopt, vopt] = optReinforce(func, rx, nt, opt)
% REINFORCE optimisation algorithm
% func = reward function handle
% rx = variable range
% nt = max # timesteps

if ~isfield(opt,'eta'); opt.eta = 0.1; end      % Learning rate
if ~isfield(opt,'alpha'); opt.alpha = 0; end    % Momentum rate
if ~isfield(opt,'gamma'); opt.gamma = 0.5; end  % Reward tracking rate
if ~isfield(opt,'m'); opt.m = 1000; end         % Number of actions
if ~isfield(opt,'onplot'); opt.onplot = 1; end  % Default show plot 

d = size(rx,2);         % Number of dimensions
b = ceil(log2(opt.m));  % Number of bits needed for m actions
n = d*b;                % Number of binary output units for d*b bits

% Initialise exponential weights
s = (1/2).^(1:b)';

% Initialize run
[rhat, yhat] = deal(0);
w = ones(n,1);
dw = zeros(n,1);
xopt = nan(d,nt);
yopt = nan(1,nt);
vopt = nan(2,nt);

% Iteratively apply REINFORCE algorithm...
for t = 1:nt
    
    % Generate random sample
    p = logsig(w);
    y = rand(n,1) < p;
    
    % Transform to constrained range
    x = rx(1,:)' + (rx(2,:)' - rx(1,:)') .* ( reshape(y,b,d)'*s );
    
    % Sample function
    if nargout(func)==1; r = func(x'); v = nan; 
    else [r,v] = func(x'); end
    
    % Update parameters
    rhat = opt.gamma*r + (1 - opt.gamma)*rhat;
    yhat = opt.gamma*y + (1 - opt.gamma)*yhat;
    dw = opt.eta*(r - rhat)*(y - p) + opt.alpha*dw;         % Variant 1
%     dw = eta*(r - rhat)*(y - yhat) + alpha*dw;      % Variant 2
    w = w + dw;
    
    % Update return values
    xopt(:,t) = x;
    yopt(t) = r;
    vopt(:,t) = v;
    
    % Plot sampled points 
    if d == 2 && rem(t-1,50) == 1 && opt.onplot
        plot_2D(xopt, yopt, rx, t)
    end
    if d == 1 && rem(t-1,50) == 1 && opt.onplot
        plot_1D(xopt, yopt, rx, t)
    end
end

end

function plot_2D(xmu, fmu, rx, t)

plot(xmu(1,:), xmu(2,:), '+k'); hold on
plot(xmu(1,end), xmu(2,end), '.r', 'markersize', 30); hold off
axis([rx(1,1) rx(2,1) rx(1,2) rx(2,2)]);
xlabel('x(1)'); ylabel('x(2)');
title(sprintf('Trial #%d', t));
drawnow;

end

function plot_1D(xmu, fmu, rx, t)

plot(xmu, fmu, '+k'); hold on
plot(xmu(end), fmu(end), '.r', 'markersize', 30); hold off
axis([rx(1,1) rx(2,1) min(fmu) 0]);
xlabel('x(1)'); ylabel('r');
title(sprintf('Trial #%d', t));
drawnow;

end