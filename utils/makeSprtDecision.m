function [selHyp, decTime, errorType] = makeSprtDecision(Hyp, thresh, varargin)
%makeSprtDecision Applies SPRT decision rule to evidence in Hyp.
%
%   Keyword input arguments:
%
%   Hyp    -- Data structure with pregenerated evidence field
%
%   thresh -- Array with evidence threshold values
%
%
%   Keyword optional 'name' 'value' pairs:
%
%   'decisions'   -- 'all' apply SPRT rule to all trial in Hyp
%                    'one' apply SPRT rule to one trial in Hyp
%
%   'trialNumber' -- Scalar number. Applies only when the 'one' value for
%                    'decisions' is chosen to indicate which trial to make
%                    decision upon
%
%   Example usage:
%      [selHyp, decTime] = makeSprtDecision(Hyp, thresh, 'decisions',
%      'one', 'trialNumber', 1)

defaultNumDecisions = 'all';
defaultIdxTrial = '1';

p = inputParser;
addRequired(p,'Hyp');
addRequired(p, 'thresh');
addOptional(p, 'decisions', defaultNumDecisions);
addOptional(p, 'trialNumber', defaultIdxTrial);
parse(p, Hyp, thresh, varargin{:});
matchedStr = validatestring(p.Results.decisions, {'all', 'one'});

if strcmp(matchedStr, 'all')
    % Initialise decision and decision time arrays.
    [selHyp, decTime] = deal(nan(p.Results.Hyp.nTrials, 1));
    errorType = nan(p.Results.Hyp.nTrials, 2);
    iterDecisions = 1:p.Results.Hyp.nTrials;
    disp('making decisions...')
else
    iterDecisions = p.Results.trialNumber;
end

% Setup thresh matrix correctly for iterations.
if length(p.Results.thresh) == 2
    thresh = ones(p.Results.Hyp.nTrials, 2).*p.Results.thresh;
else
    thresh = p.Results.thresh;
end

% Iterate trials applying SPRT stopping rule.
for i = iterDecisions
    
    % Compare evidence with thresholds.
    aMask = find(p.Results.Hyp.evidence(2:end, i) < thresh(i, 1), 1, 'first');
    bMask = find(p.Results.Hyp.evidence(2:end, i) > thresh(i, 2), 1, 'first');
    
    % Set index depending on 'decisions' optional input
    switch p.Results.decisions
        case 'all'
            idxStore = i;
        case 'one'
            idxStore = 1;
    end
    
    % Apply SPRT stopping rule and select hypothesis
    if isempty(aMask) && isempty(bMask)
        error('Threshold not exceeded before maxT')
        decTime(idxStore) = NaN; selHyp(idxStore) = NaN;
    elseif isempty(aMask) && ~isempty(bMask)
        decTime(idxStore) = bMask; selHyp(idxStore) = 2;
    elseif ~isempty(aMask) && isempty(bMask)
        decTime(idxStore) = aMask; selHyp(idxStore) = 1;
    else
        [tmp, selHyp(idxStore)] = min([aMask bMask]);
        decTime(idxStore) = tmp;
    end
    
    % Calculate error type vector
    if isnan(selHyp(idxStore))
        errorType(idxStore, :) = [NaN NaN];
    elseif selHyp(idxStore) == Hyp.target(i)
        errorType(idxStore, :) = [0, 0];
    elseif selHyp(idxStore) == 1 && Hyp.target(i) == 2
        errorType(idxStore, :) = [0, 1];
    elseif selHyp(idxStore) == 2 && Hyp.target(i) == 1
        errorType(idxStore, :) = [1, 0];
    end
    
end


