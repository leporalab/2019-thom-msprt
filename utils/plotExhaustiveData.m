function [] = plotExhaustiveData(SamplingOut)
%plotExhaustiveData Summary of this function goes here
%   Detailed explanation goes here

% Plot sampling optimiser
f3 = figure;
set(f3, 'color','white')
plot(SamplingOut.threshold, SamplingOut.reward, 'k.')
xlabel('threshold'); ylabel('reward')
hold on
[sortedThreshold, indices] = sort(SamplingOut.threshold);
sortedRewardPred = SamplingOut.rewardPred(indices);
plot(sortedThreshold, sortedRewardPred, 'r', 'LineWidth', 1.5);
v1 = vline(SamplingOut.optThresh,'r:');
v1.LineWidth = 1.5;
end

