function [f1, f2] = plotReinforceVarsAver(PlotData, plotType)
%PLOTREINFORCEVARSAVER
%   Plot learning curves averaged over however many episodes in PlotData

% Calculate averages
errorAve = mean(PlotData.error, 1);
decisionTimeAve = mean(PlotData.decTime, 1);
rewardAve = mean(PlotData.reward, 1);
thresholdAve = mean(PlotData.thresh, 1);
meanAve = mean(PlotData.mean, 1);
sdAve = mean(PlotData.sd, 1);

trials = 1:PlotData.nTrials;

% Plot figures.
f1 = figure('Color', 'white');
numPlots = 4;
subplot(numPlots,1,1)
hold on
plot(trials, errorAve )
ylim([0 1])
title('Decision error'); ylabel('Decision error, e');
subplot(numPlots,1,2)
hold on
plot(trials, decisionTimeAve )
title('Decision time'); ylabel('Decision time, T');
subplot(numPlots,1,3)
hold on
plot(trials, rewardAve )
title('Reward'); ylabel('Reward, r');
subplot(numPlots,1,4)
hold on
plot(trials, thresholdAve );
title('Decision threshold'); xlabel('trials, N'); ylabel('Decision thresholds, \theta');
%hline(ini_sprtBounds(1),'b:');

if strcmp(plotType, 'sprt')
    legend('Lower bound', 'Upper bound')
    optThresh = mean(PlotData.thresh(:, end-100:end), 2);
    t1 = annotation('textbox');
    t1.String = sprintf('REINFORCE lower bound = %.2f \nREINFORCE upper bound = %.2f',...
        [optThresh(1), optThresh(2)]);
elseif strcmp(plotType, 'msprt')
    optThresh = mean(PlotData.thresh(:, end-100:end), 2);
    meanOptThresh = mean(optThresh);
    t1 = annotation('textbox');
    t1.String = sprintf('REINFORCE MSPRT bound = %.2f', meanOptThresh);
end

t2 = annotation('textbox');
t2.String = sprintf('Averaged over %.0f episodes', PlotData.nEpisodes);

f2 = figure;
subplot(2,1,1)
hold on
plot(trials, meanAve);
title('Gaussian \mu'); ylabel('\mu');
legend('\mu_0', '\mu_1')
subplot(2,1,2)
hold on
plot(trials, sdAve);
title('Gaussian \sigma'); ylabel('\sigma');
legend('\sigma_0', '\sigma_1')

%% Plot options
f1.Color = 'white';
f1.Units               = 'centimeters';
f1.Position(1) = 2;
f1.Position(2) = 2;
%f1.Position(3)         = 18.5; % two columns
f1.Position(3)         = 9; % one column
f1.Position(4)         = 18;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);
set(t1, 'FontName', 'Arial', 'FontSize', 8,...
    'Position', [0.2365    0.0102    0.5374    0.0523]);
set(t2, 'FontName', 'Arial', 'FontSize', 8,...
    'Position', [.2530 0.9497 0.5644 0.0397]);
set(gcf, 'Renderer', 'painters')


end

