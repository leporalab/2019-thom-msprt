function [evidence, target] = generateMsprtEvidence(Hyp, evidenceType)
%GenerateMsprtEvidence Generate MSPRT evidence trajectories and targets.
%   [evidence, target] = generateMsprtEvidence(Hyp, evidenceType) returns evidence
%   trajectories (according to evidenceType={'logpost', 'logodds'}) for all hypotheses as a matrix. Target indicates the
%   true hypothesis. Dist is a distribution structure.

disp('generating MSPRT evidence trajectories...')

% Apply Distribution defaults if not user-defined
if ~isfield(Hyp,'mean'); Hyp.mean = 1; end
if ~isfield(Hyp,'sd'); Hyp.sd = 1; end
if ~isfield(Hyp,'nHyp'); Hyp.nHyp = length(Hyp.mean); end
if ~isfield(Hyp,'prior'); Hyp.prior = ones( 1, Hyp.nHyp ) / Hyp.nHyp; end
if ~isfield(Hyp,'nTrials'); Hyp.nTrials = 1e5; end
if ~isfield(Hyp,'maxT'); Hyp.maxt = 100; end

% Randomly select targets (true hypotheses)
[~, nTargets] = size(Hyp.mean);
target = randi(nTargets, [Hyp.nTrials, 1]);

% Initialise evidence storage arrays
evidence = nan(Hyp.maxT+1, Hyp.nHyp, Hyp.nTrials);
xLikelihood = nan(Hyp.maxT+1, Hyp.nHyp);
time = 0:Hyp.maxT;

% Generate evidence trajectories for true hypothesis for iTrial
for iTrial = 1:Hyp.nTrials
    xLikelihood(1,:) = Hyp.prior;
    x = mvnrnd(Hyp.mean(target(iTrial), :), Hyp.sd, Hyp.maxT);
    for iHyp = 1:Hyp.nHyp
        xLikelihood(2:end,iHyp) = mvnpdf(x, Hyp.mean(iHyp,:), Hyp.sd);  
    end
    logLikelihood = log(xLikelihood);
    logLikeTraj = cumsum(logLikelihood,1);
    logMargTraj = arrayfun(@(n) logsumexp(logLikeTraj(n,:)), 1:size(logLikeTraj, 1)); 
    if strcmp(evidenceType, 'logpost')
        evidence(:, :, iTrial) = logLikeTraj - repmat(logMargTraj', 1, size(logLikeTraj,2)); % log posterior column indexed by hypotheses (always negative)
    elseif strcmp(evidenceType, 'logodds')
        posterior = exp(logLikeTraj - repmat(logMargTraj', 1, size(logLikeTraj,2)));
        odds = (posterior) ./ (1 - posterior);
        evidence(:, :, iTrial)  = log(odds); % log odds column indexed by hypotheses 
    end
end
