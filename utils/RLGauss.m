classdef RLGauss
    %RLGauss REINFORCE with context-sensitive baseline for linear
    %associative network with Gaussian output units.
    %   Detailed explanation goes here
    
    properties
        numBounds
        thetaMean
        thetaSd
        baselineTab = zeros(100, 1);
        learningRate = 5e-4;
        learningRateW
        gamma = 0.5;
        prevContexts
        baselineType
        valFuncWeights
    end
    
    methods
        function obj = RLGauss(numBounds, sizeInputLayer, varargin)
            %LinAsNet Construct an instance of this class
            %   Detailed explanation goes here
            
            % Instantiate inputParser
            p = inputParser;
            
            % Setup parsing schema
            addRequired(p, 'numBounds')
            addRequired(p, 'sizeInputLayer')
            addParameter(p, 'baselineType', 'linValFunc') % or 'expAv'
            addParameter(p, 'learningRateW', 5e-4)
            addParameter(p, 'thetaMean0', 1)
            addParameter(p, 'thetaSd0', 0.1)
            
            % Parse inputs
            parse(p, numBounds, sizeInputLayer, varargin{:})

            obj.numBounds = p.Results.numBounds;
            obj.baselineType = p.Results.baselineType;
            obj.thetaMean = p.Results.thetaMean0.*ones(p.Results.numBounds, p.Results.sizeInputLayer);
            obj.thetaSd = p.Results.thetaSd0.*ones(p.Results.numBounds, p.Results.sizeInputLayer)/p.Results.sizeInputLayer;
            obj.valFuncWeights = zeros(1, p.Results.sizeInputLayer);
            obj.prevContexts = nan(1, p.Results.sizeInputLayer);
            obj.learningRateW = p.Results.learningRateW;        
        end
        
              function [mu, sigma] = calcMuSig(obj, contextInput)
            %METHOD1 Summary
            %   Detailed explanation here
            
            % Initialise mu and sigma vectors.
            [mu, sigma] = deal(zeros(1, obj.numBounds));
            
            % Calculate mean and sd using context and network parameters.
            for i = 1:obj.numBounds
                mu(i) = dot(obj.thetaMean(i,:), contextInput);
                sigma(i) = exp(dot(obj.thetaSd(i,:), contextInput));
            end
        end
        
        function valFunc = calcValFunc(obj, contextInput)
            valFunc = dot(obj.valFuncWeights, contextInput);
        end
        
        function [obj, contextIdx] = chkContextIdx(obj, contextInput)
            %METHOD Summary
            %   Detailed explanation here
            
            %Check if contextInput has been experienced previously
            [isSeen, contextIdx] = ismember(contextInput, obj.prevContexts, 'rows');
            if ~isSeen && unique(isnan(obj.prevContexts))
                obj.prevContexts = contextInput;
                contextIdx = 1;
            elseif ~isSeen
                %Otherwise append new contextInput and return index for new
                %row in prevContexts
                obj.prevContexts = cat(1, obj.prevContexts, contextInput);
                contextIdx = size(obj.prevContexts, 1);
            end
        end
        
        
        
        function bounds = forward(obj, contextInput)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            
            % Calculate Gaussian parameters from context
            [mu, sigma] = calcMuSig(obj, contextInput);
            
            % Sample decision boundaries
            bounds = normrnd(mu, sigma);
        end
        
        function obj = reinforce(obj, reward, contextInput, contextIdx, sampledAction)
            %METHOD2 Summary
            %   Detailed explanation here
            
            % Calculate Gaussian paramters from context
            [mu, sigma] = calcMuSig(obj, contextInput);
            
            % Set individual mean and sd learning rates as suggested in Williams (1992).
            learningRateMean = obj.learningRate*sigma.^2;
            learningRateSd = obj.learningRate*sigma.^2;
            
            % Set all baselines to be equal.
            if strcmp(obj.baselineType, 'expAv')
                baselineMean = obj.baselineTab(contextIdx);
                baselineSd = obj.baselineTab(contextIdx);
            elseif strcmp(obj.baselineType, 'linValFunc')
                baselineMean = calcValFunc(obj, contextInput);
                baselineSd = calcValFunc(obj, contextInput);
            end
            
            
            % Calculate eligibility vectors.
            [eVecMu, eVecSig] = deal(zeros(obj.numBounds));
            for i = 1:obj.numBounds
                eVecMu(i,:) = (1/sigma(i)^2).*(sampledAction(i) - mu(i))*contextInput;
                eVecSig(i,:) = (((sampledAction(i) - mu(i))^2) / (sigma(i)^2) - 1)*contextInput;
            end
            
            % Calculate deltas
            
            deltaThetaMean = learningRateMean.*(reward - baselineMean).*eVecMu;
            deltaThetaSd = learningRateSd.*(reward - baselineSd).*eVecSig;
            
            % Update thetaMean and thetaSd
            obj.thetaMean = obj.thetaMean + deltaThetaMean;
            obj.thetaSd = obj.thetaSd + deltaThetaSd;
            
        end
        
        function obj = updateBaseline(obj, reward, contextIdx, contextInput)
            if strcmp(obj.baselineType, 'expAv')
                obj.baselineTab(contextIdx) = obj.gamma*reward + ...
                    (1 - obj.gamma)*obj.baselineTab(contextIdx);
            elseif strcmp(obj.baselineType, 'linValFunc')
                obj.valFuncWeights = obj.valFuncWeights + ...
                    obj.learningRateW.*(reward - calcValFunc(obj, contextInput)).*contextInput;
            end
        end
        
    end
end




