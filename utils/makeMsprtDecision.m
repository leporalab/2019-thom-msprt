function [selHyp, decTime] = makeMsprtDecision(Hyp, thresh, varargin)
%makeSprtDecision Applies SPRT decision rule to evidence in Hyp.
%
%   Keyword input arguments:
%
%   Hyp    -- Data structure with pregenerated evidence field
%
%   thresh -- Array with evidence threshold values
%
%
%   Keyword optional 'name' 'value' pairs:
%
%   'decisions'   -- 'all' apply SPRT rule to all trial in Hyp
%                    'one' apply SPRT rule to one trial in Hyp
%
%   'trialNumber' -- Scalar number. Applies only when the 'one' value for
%                    'decisions' is chosen to indicate which trial to make
%                    decision upon
%
%   Example usage:
%      [selHyp, decTime] = makeSprtDecision(Hyp, thresh, 'decisions',
%      'one', 'trialNumber', 1)

defaultNumDecisions = 'all';
defaultIdxTrial = '1';

p = inputParser;
addRequired(p,'Hyp');
addRequired(p, 'thresh');
addOptional(p, 'decisions', defaultNumDecisions);
addOptional(p, 'trialNumber', defaultIdxTrial);
parse(p, Hyp, thresh, varargin{:});
matchedStr = validatestring(p.Results.decisions, {'all', 'one'});

if strcmp(matchedStr, 'all')
    % Initialise decision and decision time arrays.
    [selHyp, decTime] = deal(nan(p.Results.Hyp.nTrials, 1));
    errorType = nan(p.Results.Hyp.nTrials, 2);
    iterDecisions = 1:p.Results.Hyp.nTrials;
    disp('making decisions...')
else
    iterDecisions = p.Results.trialNumber;
end

% Setup thresh matrix correctly for iterations.
thresh = p.Results.thresh;
evidence = p.Results.Hyp.evidence;

isError = nan(1, length(iterDecisions));

% Iterate trials applying SPRT stopping rule.
for i = iterDecisions
    
    % Compare evidence with thresholds.
    mask = arrayfun(@(n) find(evidence(2:end, n, i) > thresh(n), 1, 'first'), ...
        1:size(evidence,2), 'UniformOutput', false);
    
    % Set index depending on 'decisions' optional input
    switch p.Results.decisions
        case 'all'
            idxStore = i;
        case 'one'
            idxStore = 1;
    end
    
    
    % Apply MSPRT stopping rule and select hypothesis
    tmp = cellfun(@isempty, mask);
    crossingTimes = nan(2, sum(~tmp));
    if sum(tmp) == Hyp.nHyp - 1
        selHyp(idxStore) = find(~tmp, 1);
        decTime(idxStore) = mask{selHyp(idxStore)};
    elseif sum(~tmp) < 1
        error('Threshold not exceeded before maxT')
        decTime(idxStore) = NaN; selHyp(idxStore) = NaN;
    else
        idx = find(~tmp, inf);
        cnt = 1;
        for k = idx    
            crossingTimes(1, cnt) = mask{k};
            crossingTimes(2, cnt) = k;
            cnt = cnt + 1;
        end
        [decTime(idxStore), I] = min(crossingTimes(1,:), [], 2);
        minMask = crossingTimes(1,:) == decTime(idxStore);
        if sum(minMask) > 1
            [~, Imx] = max(evidence(decTime(idxStore)+1, :, i));
            selHyp(idxStore) = Imx;
        else
            selHyp(idxStore) = crossingTimes(2,I);
        end
    end
end


