function [sampledContextInputs, sampledCostRatios] = sampleInputs(Hyp, contextInputAlphabet,costRatioAlphabet)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

nContexts = length(contextInputAlphabet);
sampleIdx = randsample(1:nContexts, Hyp.nTrials, true);
sampledContextInputs = zeros(Hyp.nTrials, length(contextInputAlphabet{1}));
sampledCostRatios = zeros(Hyp.nTrials, length(costRatioAlphabet{1}));
for i = 1:Hyp.nTrials
    sampledContextInputs(i, :) = contextInputAlphabet{sampleIdx(i)};
    sampledCostRatios(i, :) = costRatioAlphabet{sampleIdx(i)};
end

end

