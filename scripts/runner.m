clear
close all
%% CHOOSE PARAMETER VALUES
tic
% GENERATE HYPOTHESES ABOUT ENVIRONMENT
% noise pdf parameters
noiseMu = -1/3;
noiseSd = 1;

% Set discriminability of signal and noise
%discrim = .;
signalSd = noiseSd;

% Set pdf parameters for hypotheses
%signalMu = discrim * noiseSd + noiseMu;
signalMu = 1/3;
Hypotheses(1).mu = noiseMu;
Hypotheses(1).sd = noiseSd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = signalMu;
Hypotheses(2).sd = signalSd;
Hypotheses(2).name = 'normal';

% Set hypotheses priors
priors = [0.5, 0.5];
hypothesesCmf = cumsum(priors);

% Set maximum number of observations allowed
maxT = 1e5;

% Initialise theshold values in logLR units
%ini_sprtBounds = [ 0, 0 ]; % Use array if not constrained by equal cost ratios
ini_sprtBounds = 0;

% Initialise parameters for Gaussian parameterization
theta_mean = ini_sprtBounds;
theta_sd = zeros(1, length(ini_sprtBounds));
GaussMean = ini_sprtBounds;
GaussSd = exp(theta_sd);

%% Parameters of interest
% Set cost for type I and II errors
%costRatios = [ 100 , 100 ]; % (W0/c, W1/c), decision error costs/sampling cost
costRatios = 100;
% samplingCost = max(costRatios); 
% errorCosts = costRatios

% Set number of learning episodes
numTrials = 5000;

% Set learning parameters
learningRate = 5e-4;
baseline = 0;
gamma = 0.5;

%%
reward = 0;

% Initialise storage arrays
sprtBoundsStorageArray  =  zeros( length( ini_sprtBounds ), numTrials );
rewardStorageArray = zeros(1, numTrials);
errorStorageArray = zeros(1, numTrials);
decisionTimeStorageArray = zeros(1, numTrials);
meanStorageArray = zeros( length( ini_sprtBounds ), numTrials );
sdStorageArray = zeros( length( ini_sprtBounds ), numTrials );

%% Iterative learning of optimal decision
sprtBounds = ini_sprtBounds;
for trialIndex = 1:numTrials
    % Calculate baseline using exponential average as suggested in Williams
    % (1992). Comment this line to remove baseline.
    baseline = gamma * reward + ( 1 - gamma ) * baseline;
    
    % Set true hypothesis
    hTrue = find(rand(1) < hypothesesCmf, 1, 'first');
    
    % run one sprt simulation
    [ selHyp, decVarTimeCourse, decisionTime, errorType ] = sprt( Hypotheses, hTrue, maxT, sprtBounds );
    
    % calculate reward (inner prodect of errorType and error costs plus
    % total sampling cost)
    reward = - dot( costRatios, errorType ) - decisionTime;
    
    % Update Gaussian unit parameters (similar to weights in network)
    [ theta_mean, theta_sd ] = reinforce_gaussian( reward, learningRate, sprtBounds, baseline, GaussMean, GaussSd, theta_mean, theta_sd );
    
    % Calculate parameterized Gaussian mean and sd
    GaussMean = theta_mean;
    GaussSd = exp(theta_sd);
    
    % Sample new threshold values
    for threshIndex = 1:2
        sprtBounds(threshIndex) = icdf( 'normal', rand(1), GaussMean(threshIndex), GaussSd(threshIndex) );
    end
    
    % Capture simulation output in storage arrays
    isError = ~isempty(find(errorType, 1));
    sprtBoundsStorageArray(:, trialIndex) = sprtBounds';
    decisionTimeStorageArray(trialIndex) = decisionTime;
    rewardStorageArray(trialIndex) = reward;
    errorStorageArray(trialIndex) = isError;
    meanStorageArray(:, trialIndex) = GaussMean';
    sdStorageArray(:, trialIndex) = GaussSd';
    
    disp(trialIndex)
    
    if trialIndex == floor(numTrials/2)
        costRatios = costRatios/10;
    end
        
end

% Display final values (averaged over last nav trials)
nav = 100;
fprintf('Upper learned bound = %0.5g \n', mean(sprtBoundsStorageArray(2,end-nav:end)));
fprintf('Lower learned bound = %0.5g \n', mean(sprtBoundsStorageArray(1,end-nav:end)));

% smooth outputs
numTrialsSmoothing = 50;
thresholdSmoothed(1,:) = smooth(sprtBoundsStorageArray(1,:), numTrialsSmoothing);
thresholdSmoothed(2,:) = smooth(sprtBoundsStorageArray(2,:), numTrialsSmoothing);
decisionTimeSmoothed = smooth(decisionTimeStorageArray, numTrialsSmoothing);
rewardSmoothed = smooth(rewardStorageArray, numTrialsSmoothing);
errorSmoothed = smooth(errorStorageArray, numTrialsSmoothing);

%generate figures
f1 = figure; numPlots = 4;
set(f1, 'color','white')
subplot(numPlots,1,1)
plot( 1:numTrials, errorSmoothed )
title('Decision error'); ylabel('Decision error, e');
subplot(numPlots,1,2)
plot( 1:numTrials, decisionTimeSmoothed )
title('Decision time'); ylabel('Decision time, T');
subplot(numPlots,1,3)
plot( 1:numTrials, rewardSmoothed )
title('Reward'); ylabel('Reward, r');
subplot(numPlots,1,4)
plot( 1:numTrials, thresholdSmoothed )
title('Decision threshold'); xlabel('trials, N'); ylabel('Decision thresholds, \theta');
hline(ini_sprtBounds(1),'b:'); hline(ini_sprtBounds(2));

f2 = figure;
subplot(2,1,1)
plot( 1:numTrials, meanStorageArray );
title('Gaussian \mu'); ylabel('\mu');
subplot(2,1,2)
plot( 1:numTrials, sdStorageArray );
title('Gaussian \sigma'); ylabel('\sigma');


%% Plot options
f1.Units               = 'centimeters';
f1.Position(3)         = 18.5; % two columns
%f1.Position(3)         = 9; % one column
f1.Position(4)         = 5;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

l1.FontSize = 7; l1.Box = 'off'; l1.Position = [0.5359 0.7238 0.0887 0.1825];

set(gcf, 'Renderer', 'painters')
toc