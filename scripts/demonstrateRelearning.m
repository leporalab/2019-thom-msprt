clear
close all

%% Set parameters.
% Define hypotheses distributions.
Hyp.mean = [-1/3, 1/3]; % 1 index is 'noise', 2 index is 'signal'
Hyp.sd = 1;
Hyp.nHyp = 2;
Hyp.nTrials = 5e3;
Hyp.maxT = 200;

% Initialise sprt bounds.
ini_sprtBounds = [0];

% Initialise parameters for Gaussian parameterization.
GaussParam.thetaMean = ini_sprtBounds;
GaussParam.thetaSd = zeros(1, length(ini_sprtBounds));
GaussParam.mean = ini_sprtBounds;
GaussParam.sd = exp(GaussParam.thetaSd);

% Set learning parameters.
ReinParam.learningRate = 5e-4;
ReinParam.baseline = 0;
ReinParam.gamma = 0.5;
ReinParam.thresh = ini_sprtBounds;

costRatios = [100, 100]; % (W_0/c, W_1/c)
threshRange = [0, 8];

% Generate evidence and select true hypotheses (targets).
[Hyp.evidence, Hyp.target] = generateSprtEvidence(Hyp);

%% Optimise thresholds using REINFORCE (Gaussian unit).
[PlotData, optThreshReinforce, GaussParam, ReinParam] = optReinforceGaussian(costRatios, ...
    ini_sprtBounds, Hyp, ReinParam, GaussParam);

%% Change cost ratios and relearn with initial parameters values from previous optimisation

% Generate evidence and select true hypotheses (targets).
[Hyp.evidence, Hyp.target] = generateSprtEvidence(Hyp);

costRatios = [500, 500];
ini_sprtBounds = [ReinParam.thresh];
[PlotData2, optThreshReinforce2, GaussParam2, ReinParam2] = optReinforceGaussian(costRatios, ...
    ini_sprtBounds, Hyp, ReinParam, GaussParam);

%% Stitch PlotData structs together
fields = {'mean', 'sd', 'reward', 'error', 'decTime', 'thresh'};
for i = 1:length(fields)
    NewPlotData.(fields{i}) = [PlotData.(fields{i}), PlotData2.(fields{i})];
end

%% Generate REINFORCE figures.
numTrialsSmoothing = 50;
% Double number of trials for plotting
NewPlotData.nTrials = 2*Hyp.nTrials;
[f1, f2] = plotReinforceVars(NewPlotData, numTrialsSmoothing);
figure(f1)
subplot(4,1,4)
hline(mean(NewPlotData.thresh(Hyp.nTrials - 100:Hyp.nTrials)))
hline(mean(NewPlotData.thresh(NewPlotData.nTrials - 100:NewPlotData.nTrials)))





