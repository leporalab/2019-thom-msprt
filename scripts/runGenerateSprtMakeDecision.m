clear
close all

% Define hypotheses distributions
Hyp.mean = [-1/3, 1/3]; % 1 index is 'noise', 2 index is 'signal'
Hyp.sd = 1; 
Hyp.nHyp = 2;
Hyp.nTrials = 1e5;
Hyp.maxT = 100;

% Generate evidence and select true hypothesis (target)
[Hyp.evidence, Hyp.target] = generateSprtEvidence(Hyp);

% Set thresholds
thresh = [-2 2];

% Apply SPRT decision rule to evidence
[selHyp, decTime] = makeSprtDecision(Hyp, thresh,...
    'decisions', 'all',...
    'trialNumber', 1);
