p = linspace(0.001, 0.999, 100);
logP = log(p);
logOddsP = log(p./(1-p));

plot(p, logP)
hold on
plot(p, logOddsP)
legend('log posterior', 'log odds')