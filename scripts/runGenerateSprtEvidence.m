clear
close all

% Define Distributions for hypotheses
%Dist.generate = @generateLogp; 
Dist.generateFunc = @generateSprtEvidence; %@generateDDM;
Dist.mean = 1/2; Dist.sd = 1; Dist.nHyp = 2;
Dist.nTrials = 1e3;
Dist.maxT = 100;

% Generate evidence and select true hypothesis (target)
tic
[Dist.evidence, Dist.target] = Dist.generateFunc(Dist);
toc
