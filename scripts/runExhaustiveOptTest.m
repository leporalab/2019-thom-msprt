clear
close all
%% CHOOSE PARAMETER VALUES
tic
% GENERATE HYPOTHESES ABOUT ENVIRONMENT
% noise pdf parameters
noiseMu = -1/3;
noiseSd = 1;

% Set discriminability of signal and noise
%discrim = .;
signalSd = noiseSd;

% Set pdf parameters for hypotheses
%signalMu = discrim * noiseSd + noiseMu;
signalMu = 1/3;
Hypotheses(1).mu = noiseMu;
Hypotheses(1).sd = noiseSd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = signalMu;
Hypotheses(2).sd = signalSd;
Hypotheses(2).name = 'normal';

% Set maximum number of observations allowed
maxT = 1e5;

% Set hypotheses priors
priors = [0.5, 0.5];
hypothesesCmf = cumsum(priors);

%
nTrials = 1e4;
costRatio = 50;
threshRange = [0 10];

%%

% Set true hypothesis
hTrue = find(rand(1) < hypothesesCmf, 1, 'first');

[OptThresh, PlotData] = exhaustive_opt( Hypotheses, hTrue, maxT, threshRange, nTrials, costRatio );

f1 = figure;
set(f1, 'color','white')
scatter( PlotData.threshold, PlotData.reward, 'k.' )
xlabel('threshold'); ylabel('reward')

%% Plot options
f1.Units               = 'centimeters';
%f1.Position(3)         = 18.5; % two columns
f1.Position(3)         = 9; % one column
f1.Position(4)         = 5;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

l1.FontSize = 7; l1.Box = 'off'; l1.Position = [0.5359 0.7238 0.0887 0.1825];

set(gcf, 'Renderer', 'painters')
toc