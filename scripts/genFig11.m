clear
close all

%% Set parameters.
% Define hypotheses distributions.
mu = 1/3;
width = 1;
numHyps = 3;
Hyp.nHyp = numHyps;
Hyp.mean = mu.*eye(Hyp.nHyp); % mean
Hyp.sd = width.*eye(Hyp.nHyp); % Covariance matrix assuming MVN
Hyp.nTrials = 10e3;
Hyp.maxT = 1000;

%%

costRatio = 100; % Same cost for all W_{i,j} errors
contextInput = 1; % No contexts
ini_msprtBounds = 1;
msprtBounds = ini_msprtBounds;
rl = RLGauss(length(msprtBounds), length(contextInput), ...
                    'baselineType', 'linValFunc',...
                    'thetaMean0', 0,...
                    'thetaSd0', -1,...
                    'learningRateW', 5e-4);
trialCounter = zeros(100, 1);

% Generate evidence for MSPRT
[Hyp.evidence, Hyp.target] = generateMsprtEvidence(Hyp, 'logodds');

for trialIndex = 1:Hyp.nTrials
    % Sample new threshold values.
    msprtBoundSize = rl.forward(contextInput);
    msprtBounds = msprtBoundSize*ones(1, Hyp.nHyp);
    
    % Apply MSPRT decision rule to evidence indexed by trialIndex.
    [selHyp, decTime] = makeMsprtDecision(Hyp, msprtBounds,...
        'decisions', 'one',...
        'trialNumber', trialIndex);
    
    isError = ~(selHyp == Hyp.target(trialIndex));
    
    % Calculate reward.
    reward = -costRatio*isError - decTime; % Setting c=1, W_{i,j} = W
    %reward = - dot(sampledCostRatios(trialIndex, :), errorType) - decTime; % Setting c=1
    
    % Apply REINFORCE update
    rl = reinforce(rl, reward, contextInput, 1, msprtBounds);
    % Calculate baseline using exponential average as suggested in Williams
    % (1992). Comment this line to remove baseline.
    rl = rl.updateBaseline(reward, 1, contextInput);
    
    % Capture plot data indexed by context
    PlotData.thresh(trialIndex) = msprtBoundSize;
    PlotData.decTime(trialIndex) = decTime;
    PlotData.reward(trialIndex) = reward;
    PlotData.error(trialIndex) = isError;
    [mu, sig] = rl.calcMuSig(contextInput);
    PlotData.mean(trialIndex) = mu;
    PlotData.sd(trialIndex) = sig;
    
end
PlotData.nTrials = Hyp.nTrials;

%% Generate REINFORCE figures.
numTrialsSmoothing = 50;
for i = 1:length(PlotData)
    plotReinforceVars(PlotData(i), numTrialsSmoothing, 'msprt');
end

%% Optimise thresholds using exhaustive search
% threshRange = [0 8];
% for i = 1:length(costRatioAlphabet)
%     SamplingOut(i) = optExhaustive(costRatioAlphabet{i}, threshRange, Hyp);
% end



