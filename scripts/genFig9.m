clear
close all

%% Set parameters.
% Define hypotheses distributions.
Hyp.mean = [-1/3, 1/3]; % 1 index is 'noise', 2 index is 'signal'
Hyp.sd = 1;
Hyp.nHyp = 2;
Hyp.nTrials = 10e3;
Hyp.maxT = 200;



% Randomly sample context input vectors and corresponding cost-ratios
contextInputAlphabet = {[1 0], ...
    [1 1]};
costRatioAlphabet = {[100 100]};%,...
%[400 400]};
% %[sampledContextInputs, sampledCostRatios] = sampleInputs(Hyp, contextInputAlphabet, costRatioAlphabet);
% sampledContextInputs = [repmat(contextInputAlphabet{1}, Hyp.nTrials/2, 1); ...
%                         repmat(contextInputAlphabet{2}, Hyp.nTrials/2, 1)];
sampledCostRatios = repmat(costRatioAlphabet{1}, Hyp.nTrials, 1);
sampledContextInputs = repmat(contextInputAlphabet{1}, Hyp.nTrials, 1);

ini_sprtBounds = [0 0];
nEpisodes = 10;
learningRateAlphabet = [5e-3, 5e-1];
baselineAlphabet = [repmat({'linValFunc'}, length(learningRateAlphabet), 1);...
    {'expAv'}];

for learnIndex = 1:length(baselineAlphabet)
    rl = RLGauss(length(ini_sprtBounds), length(contextInputAlphabet{1}), 'baselineType', baselineAlphabet{learnIndex});
    for episodeIndex = 1:nEpisodes
        % Generate evidence and select true hypotheses (targets).
        [Hyp.evidence, Hyp.target] = generateSprtEvidence(Hyp);
        sprtBounds = ini_sprtBounds;
        trialCounter = zeros(100, 1);
        for trialIndex = 1:Hyp.nTrials
            % Assign index to context input vector.
            [rl, contextIdx] = chkContextIdx(rl, sampledContextInputs(trialIndex, :));
            % Increment counter indexed by context.
            trialCounter(contextIdx) = trialCounter(contextIdx) + 1;
            
            % Sample new threshold values.
            sprtBounds = rl.forward(sampledContextInputs(trialIndex, :));
            
            % Apply SPRT decision rule to evidence indexed by trialIndex.
            [selHyp, decTime, errorType] = makeSprtDecision(Hyp, sprtBounds,...
                'decisions', 'one',...
                'trialNumber', trialIndex);
            
            % Calculate reward.
            reward = - dot(sampledCostRatios(trialIndex, :), errorType) - decTime; % Setting c=1
            
            % Apply REINFORCE update
            
            rl = reinforce(rl, reward, sampledContextInputs(trialIndex,:), contextIdx, sprtBounds);
            % Calculate baseline using exponential average as suggested in Williams
            % (1992). Comment this line to remove baseline.
            rl = rl.updateBaseline(reward, contextIdx, sampledContextInputs(trialIndex, :));
            
            % Capture plot data indexed by context
            isError = ~isempty(find(errorType, 1));
            PlotData.thresh(:, trialCounter(contextIdx), episodeIndex) = sprtBounds;
            PlotData.decTime(episodeIndex, trialCounter(contextIdx)) = decTime;
            PlotData.reward(episodeIndex, trialCounter(contextIdx)) = reward;
            PlotData.error(episodeIndex, trialCounter(contextIdx)) = isError;
            [mu, sig] = rl.calcMuSig(sampledContextInputs(trialIndex, :));
            PlotData.mean(:, trialCounter(contextIdx), episodeIndex) = mu;
            PlotData.sd(:, trialCounter(contextIdx), episodeIndex) = sig;
            
        end
        
        
        for i = 1:size(rl.prevContexts, 1)
            PlotData(i).nTrials = trialCounter(i);
        end
        disp(episodeIndex)
    end
    AvPlotData(learnIndex).reward = mean(PlotData.reward, 1);
    AvPlotData(learnIndex).thresh = mean(PlotData.thresh, 3);
    AvPlotData(learnIndex).error = mean(PlotData.error, 1);
    AvPlotData(learnIndex).decTime = mean(PlotData.error, 1);
end

%%
f1 = figure('Color', 'white');
axes
C = get(gca, 'ColorOrder');
smoothing = 100;
lineStyleArr = {'-', '--', ':'};

for i = 1:length(baselineAlphabet)
    subplot(4,1,1)
    hold on
    plot(smooth(AvPlotData(i).error, smoothing))
    title('Decision error'); ylabel('Decision error, e');
    subplot(4,1,2)
    hold on
    plot(smooth(AvPlotData(i).decTime, smoothing))
    title('Decision time'); ylabel('Decision time, T');
    subplot(4,1,3)
    hold on
    plot(smooth(AvPlotData(i).reward, smoothing))
    title('Reward'); ylabel('Reward, r');
    subplot(4,1,4)
    for j = 1:size(AvPlotData(i).thresh, 1)
        thresholdSmoothed(j,:) = smooth(AvPlotData(i).thresh(j,:), smoothing);
    end
    hold on
    for j = 1:2
        h(i,j) = plot(thresholdSmoothed(j,:), 'color', C(j,:), 'linestyle', lineStyleArr{i});
    end
    title('Decision threshold'); xlabel('trials, N'); ylabel('Decision thresholds, \theta');
    %hline(ini_sprtBounds(1),'b:');
end
subplot(4,1,1)
legArr = [arrayfun(@num2str, learningRateAlphabet, 'UniformOutput', false), 'expAv'];
legend(legArr)

subplot(4,1,4)
legend([h(1,1) h(2,1) h(3,1)], {'0.005', '0.5', 'expAv'})

%% Plot options
f1.Color = 'white';
f1.Units               = 'centimeters';
f1.Position(1) = 2;
f1.Position(2) = 2;
%f1.Position(3)         = 18.5; % two columns
f1.Position(3)         = 9; % one column
f1.Position(4)         = 18;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);
%set(t1, 'FontName', 'Arial', 'FontSize', 8,...
%'Position', [0.2365    0.0102    0.5374    0.0523]);
set(gcf, 'Renderer', 'painters')
