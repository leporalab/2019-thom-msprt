clear
close all

%% Set parameters.
% Define hypotheses distributions.
mu = 1;
width = 1;
numHyps = [3 5];

markerArray = {'s','d'};

for iNum = 1:length(numHyps)
    Hyp.nHyp = numHyps(iNum);
    Hyp.mean = mu.*eye(Hyp.nHyp); % mean
    Hyp.sd = width.*eye(Hyp.nHyp); % Covariance matrix assuming MVN
    Hyp.nTrials = 1e3;
    Hyp.maxT = 100;
    
    % Range of thresholds for speed-accuracy curve
    threshSize = linspace(1/Hyp.nHyp + 0.1, 0.99, 20);
    
    %% Generate evidence and select true hypotheses (targets).
    tic
    
    [errorRate, meanDecTime, threshStore] = deal(nan(length(threshSize),1));
    for i = 1:length(threshSize)
        thresh = log(threshSize(i)*ones(1, Hyp.nHyp) ./ (1 - threshSize(i)*ones(1, Hyp.nHyp)));
        
        [Hyp.evidence, Hyp.target] = generateMsprtEvidence(Hyp, 'logodds');
        
        % Apply SPRT decision rule to evidence indexed by trialIndex.
        [selHyp, decTime] = makeMsprtDecision(Hyp, thresh,...
            'decisions', 'all');
        
        error = ~(selHyp == Hyp.target);
        errorRate(i) = sum(error)/Hyp.nTrials;
        meanDecTime(i) = mean(decTime);
        threshStore(i) = threshSize(i);
    end
    %% PLOT
    if iNum == 1
        f1 = figure;
        set(f1, 'color','white', 'position', [100 100 550 350])
        C = get(gca,'colororder'); % set attractive colour order
    end
    % Fit logistic regression model to error and n data
    % B = glmfit(t.n, [t.error ones(length(t.error), 1)], 'binomial', 'link', 'logit');
    % X = linspace(0, max(t.n), 100);
    % Z = Logistic(B(1) + X*B(2));
    
    % jitterAmount = 0.01;
    % %jitterValuesX = 2*(rand(size(dataX))-0.5)*jitterAmount;   % +/-jitterAmount max
    % jitter = 2*(randn(size(t.error))-0.5)*jitterAmount;   % +/-jitterAmount max
    % %scatter(dataX+jitterValuesX, dataY+jitterValuesY);
    
    scatter(meanDecTime, errorRate, 20, threshStore, markerArray{iNum}, 'filled');
    hold on
    %plot(X, Z, 'linewidth', 1.2)
    
end
c = colorbar; ylabel(c, 'threshold, \theta'); c.FontSize = 8;
xlabel('mean decision time'); ylabel('error rate');
ylim([-0.1 0.5])
box on; grid on
title('MSPRT speed-accuracy curve (equal thresholds)')

%% Format figure
f1.Units               = 'centimeters';
f1.Position(3)         = 9;
f1.Position(4)         = 6.5;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

% l1.FontSize = 7;
% l2.FontSize = 7;

set(gcf, 'Renderer', 'painters')
pos = get(f1,'Position');
set(f1,'PaperPositionMode','Auto','PaperUnits','Centimeters','PaperSize',[pos(3), pos(4)])


%%
% Randomly sample context input vectors and corresponding cost-ratios
contextInputAlphabet = {[1 0], ...
    [1 1]};
costRatioAlphabet = {[100 100],...
    [400 400]};
%[sampledContextInputs, sampledCostRatios] = sampleInputs(Hyp, contextInputAlphabet, costRatioAlphabet);
sampledContextInputs = [repmat(contextInputAlphabet{1}, Hyp.nTrials/2, 1); ...
    repmat(contextInputAlphabet{2}, Hyp.nTrials/2, 1)];
sampledCostRatios = repmat(costRatioAlphabet{1}, Hyp.nTrials, 1);

ini_sprtBounds = [0 0];
sprtBounds = ini_sprtBounds;
rl = RLGauss(length(sprtBounds), length(contextInputAlphabet{1}));
trialCounter = zeros(100, 1);

for trialIndex = 1:Hyp.nTrials
    % Assign index to context input vector.
    [rl, contextIdx] = chkContextIdx(rl, sampledContextInputs(trialIndex, :));
    % Increment counter indexed by context.
    trialCounter(contextIdx) = trialCounter(contextIdx) + 1;
    
    % Sample new threshold values.
    sprtBounds = rl.forward(sampledContextInputs(trialIndex, :));
    
    % Apply SPRT decision rule to evidence indexed by trialIndex.
    [selHyp, decTime, errorType] = makeSprtDecision(Hyp, sprtBounds,...
        'decisions', 'one',...
        'trialNumber', trialIndex);
    
    % Calculate reward.
    reward = - dot(sampledCostRatios(trialIndex, :), errorType) - decTime; % Setting c=1
    
    % Apply REINFORCE update
    
    rl = reinforce(rl, reward, sampledContextInputs(trialIndex,:), contextIdx, sprtBounds);
    % Calculate baseline using exponential average as suggested in Williams
    % (1992). Comment this line to remove baseline.
    rl = rl.updateBaseline(reward, contextIdx, sampledContextInputs(trialIndex, :));
    
    % Capture plot data indexed by context
    isError = ~isempty(find(errorType, 1));
    PlotData(contextIdx).thresh(:, trialCounter(contextIdx)) = sprtBounds;
    PlotData(contextIdx).decTime(trialCounter(contextIdx)) = decTime;
    PlotData(contextIdx).reward(trialCounter(contextIdx)) = reward;
    PlotData(contextIdx).error(trialCounter(contextIdx)) = isError;
    [mu, sig] = rl.calcMuSig(sampledContextInputs(trialIndex, :));
    PlotData(contextIdx).mean(:, trialCounter(contextIdx)) = mu;
    PlotData(contextIdx).sd(:, trialCounter(contextIdx)) = sig;
    
end

for i = 1:size(rl.prevContexts, 1)
    PlotData(i).nTrials = trialCounter(i);
end


%% Generate REINFORCE figures.
% numTrialsSmoothing = 100;
% for i = 1:length(PlotData)
%     plotReinforceVars(PlotData(i), numTrialsSmoothing);
% end

%% Optimise thresholds using exhaustive search
% threshRange = [0 8];
% for i = 1:length(costRatioAlphabet)
%     SamplingOut(i) = optExhaustive(costRatioAlphabet{i}, threshRange, Hyp);
% end


%% What happens with novel context?
% Generate evidence and select true hypotheses (targets).
Hyp.nTrials = 100;

[Hyp.evidence, Hyp.target] = generateSprtEvidence(Hyp);

novelContext = [0 1];
oldContext = [1 0];
sampledContextInputs = repmat(novelContext, Hyp.nTrials, 1);
sampledCostRatios = 100.*ones(Hyp.nTrials, 2);

for trialIndex = 1:Hyp.nTrials
    % Assign index to context input vector.
    [rl, contextIdx] = chkContextIdx(rl, sampledContextInputs(trialIndex, :));
    % Increment counter indexed by context.
    trialCounter(contextIdx) = trialCounter(contextIdx) + 1;
    
    % Sample new threshold values.
    sprtBounds = rl.forward(sampledContextInputs(trialIndex, :));
    
    % Apply SPRT decision rule to evidence indexed by trialIndex.
    [selHyp, decTime, errorType] = makeSprtDecision(Hyp, sprtBounds,...
        'decisions', 'one',...
        'trialNumber', trialIndex);
    
    % Calculate reward.
    reward = - dot(sampledCostRatios(trialIndex, :), errorType) - decTime; % Setting c=1
    
    %     % Apply REINFORCE update
    %     rl = reinforce(rl, reward, sampledContextInputs(trialIndex,:), contextIdx, sprtBounds);
    %     % Calculate baseline using exponential average as suggested in Williams
    %     % (1992). Comment this line to remove baseline.
    %     rl = rl.updateBaseline(reward, contextIdx);
    
    % Capture plot data indexed by context
    isError = ~isempty(find(errorType, 1));
    PlotData(contextIdx).thresh(:, trialCounter(contextIdx)) = sprtBounds;
    PlotData(contextIdx).decTime(trialCounter(contextIdx)) = decTime;
    PlotData(contextIdx).reward(trialCounter(contextIdx)) = reward;
    PlotData(contextIdx).error(trialCounter(contextIdx)) = isError;
    [mu, sig] = rl.calcMuSig(sampledContextInputs(trialIndex, :));
    PlotData(contextIdx).mean(:, trialCounter(contextIdx)) = mu;
    PlotData(contextIdx).sd(:, trialCounter(contextIdx)) = sig;
    
end

for i = 1:size(rl.prevContexts, 1)
    PlotData(i).nTrials = trialCounter(i);
end

%% Generate REINFORCE figures.
numTrialsSmoothing = 50;
for i = 1:2
    plotReinforceVars(PlotData(i), numTrialsSmoothing);
end

%% Plot threshold data
figure('color','white')
C = {'blue', 'red'}; L = {'-', ':'};
numTrialsSmoothing = 100;
for j = 1:length(PlotData)
    trials = 1:PlotData(j).nTrials;
    thresholdSmoothed = zeros(2, PlotData(j).nTrials);
    for i = 1:size(PlotData(j).thresh, 1)
        thresholdSmoothed(i,:) = smooth(PlotData(j).thresh(i,:), numTrialsSmoothing)';
        plot(trials, thresholdSmoothed(i,:), 'color', C{i}, 'linestyle', L{j});
        hold on
    end
    
end
title('Decision threshold'); xlabel('trials, N'); ylabel('Decision thresholds, \theta');
hline(SamplingOut(1).optThresh,'k-');
hline(SamplingOut(2).optThresh,'k:');
%legend('Lower bound', 'Upper bound')

%%
% Plot figures.
trials = 1:PlotData(3).nTrials;
numTrialsSmoothing = 20;
for i = 1:size(PlotData(3).thresh, 1)
    thresholdSmoothed(i,:) = smooth(PlotData(3).thresh(i,:), numTrialsSmoothing);
end
decisionTimeSmoothed = smooth(PlotData(3).decTime, numTrialsSmoothing);
rewardSmoothed = smooth(PlotData(3).reward, numTrialsSmoothing);
errorSmoothed = smooth(PlotData(3).error, numTrialsSmoothing);

f1 = figure('Color', 'white');
numPlots = 4;
subplot(numPlots,1,1)
hold on
plot(trials, errorSmoothed )
title('Decision error'); ylabel('Decision error, e');
subplot(numPlots,1,2)
hold on
plot(trials, decisionTimeSmoothed )
title('Decision time'); ylabel('Decision time, T');
subplot(numPlots,1,3)
hold on
plot(trials, rewardSmoothed )
title('Reward'); ylabel('Reward, r');
subplot(numPlots,1,4)
hold on
plot(trials, thresholdSmoothed);
title('Decision threshold'); xlabel('trials, N'); ylabel('Decision thresholds, \theta');
%hline(ini_sprtBounds(1),'b:');
legend('Lower bound', 'Upper bound')

%% Plot options
f1.Color = 'white';
f1.Units               = 'centimeters';
f1.Position(1) = 2;
f1.Position(2) = 2;
%f1.Position(3)         = 18.5; % two columns
f1.Position(3)         = 9; % one column
f1.Position(4)         = 18;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);
%set(t1, 'FontName', 'Arial', 'FontSize', 8,...
%'Position', [0.2365    0.0102    0.5374    0.0523]);
set(gcf, 'Renderer', 'painters')

